# **APP-FUNGAME**
Projet de jeu de rôle structuré en microservices et développé en Python.


## *1) Structure du dépôt*
   - une branche correspond à un microservice.
   - pour lister les branches existantes :
       ```console
       $ git ls-remote
       ```

### Branches de développement
   - chaque branche de développement porte le nom de son service.
   - chaque branche peut être modifiée par les développeurs du service.
 

## *2) Consignes pour développeurs*

### Organisation des fichiers
En local:
   - créer une nouvelle branche à partir de la branche `master`. Donner à la branche le nom du service,
     sans espaces et sans majuscules (les tirets sont autorisés).
   - créer un fichier `requirements.txt` à la racine qui contient la liste des plugins 
     nécessaires pour exécuter votre service en utilisant la commande :
        ```console
        $ pip freeze >requirements.txt
        ```
   - respecter les nomenclatures suivantes pour les fichiers et dossiers :
        * un fichier `run.py` à la racine pour lancer le service
        * un dossier `service` qui contient tous les fichiers (\*.py, \*.json, etc.) attenant au service
     
     La structure du dossier du service doit être identique à :
        ```console
        /service
        /autre-dossier-si-besoin
        autres-fichier-si-besoin
        docker-compose.yml
        Dockerfile
        Jenkinsfile
        README.md 
        requirements.txt
        ```
  
### Modifications du fichier `docker-compose.yml`
Dans le fichier `docker-compose.yml`, vous devez renseigner deux champs :
   - *ligne  8 :* le nom du service. De préférence, choisir ici un nom identique à celui de la branche
   - *ligne 12 :* le port extérieur pour accéder au service. A noter que celui-ci ne sera accessible
                  que pendant la phase de test. Merci de choisir le port avec un numéro séquentiel
                  dans le tableau présenté ci-dessous.

| service | port extérieur (phase test) |
| ------- | --------------------------- |
| bus-api-ihm | 5050 |
| personnage  | 5051 |

Par exemple, pour le service `bus-api-ihm`, le fichier prend la forme :
   ```console 
   version: '3.4'

   networks:
     fungame-subnet:
       external: true

   services:
     bus-api-ihm:
       image: "registry.gitlab.com/ajcpiroma/app-fungame/${BRANCH_NAME}:latest"
       container_name: "fg-${BRANCH_NAME}"
       ports:
        - 5050:80
       networks:
      - fungame-subnet
   ```

Une fois ces modifications effectuées, vous êtes prêt à publier votre service sur le Gitlab de l'application.

### Release candidate et génération d'une image de conteneur
Les branches sont scannées pour changement toutes les 15 minutes.
Une image n'est générée que si un tag est présent et que celui-ci commence par "rc-"
Lorsque vous êtes satisfaits de votre service vous devez :
 - créer un tag annoté pour votre service : rc-[nom-service]-[num-version] 
   par exemple pour le service bus-api-ihm en version 1.0
    ``` 
    git tag -m "message de commit" "rc-bus-api-ihm-1.0"
    ```
 - puis pousser le commit avec le tag
    ```
    git push --follow-tags
    ```